package com.team.demo;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;

//Teamwork表示类名(类名和java文件一致) class表示类  extends继承  JFrame窗口
public class Teamwork extends JFrame{
	int index = 0;//集合的索引(创建变量)
	
	//设置窗体的一些效果（窗体的标题、窗体的大小、位置等等）
	public Teamwork(){
		//this表示本窗体
		this.setTitle("学生点名");//setTitle方法设置标题 
		this.setSize(400,300);//设置窗体大小
		this.setResizable(true);//能改变窗体大小
		this.setLocationRelativeTo(null);//设置窗体在屏幕中央显示
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//关闭窗体时自动关闭程序
		this.setLayout(null);//设置自定义布局（空布局）
		init();
		this.setVisible(true);//设置窗体显示，最好放在最后
		
		getNames();
	}	
	
	JLabel JlName = null;
	Timer timer = null;
	//初始化（创建窗体中需要的控件.......）
	public void init() {
		JButton jbStart = new JButton("开始");//创建一个按钮,jbStart表示开始按钮名称 
		jbStart.setBounds(60,170,80,30); //设置按钮的位置和宽高
		this.add(jbStart);//将按钮添加到窗体中
		JButton jbStop = new JButton("结束");//创建一个按钮,jbStart表示开始按钮名称 
		jbStop.setBounds(240,170,80,30); //设置按钮的位置和宽高
		this.add(jbStop);//将按钮添加到窗体中
		
		JlName = new JLabel("");//创建一个标签
		JlName.setBounds(150,70,100,40);//设置位置和大小
		JlName.setFont(new Font(Font.DIALOG,Font.BOLD,30));//这只字体，粗细，字大小
		this.add(JlName);//将其添加到窗体
		
		//定时器（作用：每隔一段时间，就执行一次）
		timer = new Timer(50,new ActionListener() {//100表示每隔多久的时间（单位：毫秒）
			public void actionPerformed(ActionEvent e) {
				//要执行的代码
				JlName.setText(names.get(index));//设置标签上的文本
				index++;//改变索引(让index加1)
				if(index == names.size()) { //index是不是超出了最大索引  names.size()获取集合中名称的个数
					index = 0;//将index设置为0
				}
			}
		});
		
		//点击开始按钮，能让定时器运行起来
		jbStart.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				//点击的时候需要执行的代码
				timer.start();
			}
		});
		
		//点击停止按钮，能让定时器暂停
		jbStop.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				//点击的时候需要执行的代码
				timer.stop();
			}
		});
	}
	
	ArrayList<String> names = new ArrayList<String>();//创建一个集合（用来存储多个名称）
	//获取所有名称
	public void getNames() {
/*		names.add("张三");
		names.add("李四");
		names.add("王五");
		names.add("英六");
		names.add("华七");
		names.add("郭佳琪");
		names.add("陈石雨");*/
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader("names.txt"));//创建一个流
			String name = br.readLine();//从记事本中读取一行文本,将文本存储到name变量中
			while(name != null){//name != null如果成立就会循环，不过不成立循环就结束了
				//循环的代码
				names.add(name);
				name = br.readLine();
			}

		}catch(Exception e){
			e.printStackTrace();
		}finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}			
		}
	}
	
	//nain方法作用，程序的入口  
	public static void main(String[] args) {
		new Teamwork();
	}
}
